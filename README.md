# VestooApi

Provides an API to Vestoo

## Getting Started in three Steps

1. Install the gem via $ gem install vestoo-api
2. Fire up the genarator via $ gamify
3. You are ready to go

## Usage
If you want to credit a user, you just have to include the VestooApi module in
your Controller:
    $ include VestooApi

Now you can call 
    $ credit_user( cookies['vestoo_uid', cid)

