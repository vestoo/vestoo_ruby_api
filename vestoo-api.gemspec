# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'vestoo-api/version'

Gem::Specification.new do |gem|
  gem.name          = "vestoo-api"
  gem.version       = VestooApi::VERSION
  gem.platform      = Gem::Platform::RUBY
  gem.authors       = ["Manuel Kulisch", "Michael Schnelle", "Michael Gerstl"]
  gem.email         = ["info@vestoo.com"]
  gem.description   = %q{This is the first version of the vestoo-api for ruby}
  gem.summary       = %q{It is awsome}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_dependency "rest-client"
end


