require "vestoo-api/version"
require "rest-client"

module VestooApi

  RestClient.log =
  Object.new.tap do |proxy|
    def proxy.<<(message)
      Rails.logger.info message
    end 
  end

  @@env = Rails.env ? Rails.env : "development"
  @@config = YAML.load_file("config/vestoo.yml")


  @@base_url = ENV['V_HOME'] ? ENV['V_HOME'] : "http://127.0.0.1:3000"
  @@client = RestClient::Resource.new(@@base_url, :user => @@config[@@env]["app_key"], :password => @@config[@@env]["app_secret"] ,:content_type => :json, :accept => :json)




  def credit_user(user_id, challenge_id, options = {})
    handle_rest_exception {
      credit_user!(user_id, challenge_id, options)
    }
  end

  def credit_user!(user_id, challenge_id, options = {})
    puts @@client.inspect
    @@client['/api/rest/challenges'].post(uid: user_id, cid: challenge_id, options: options)
  end



  private

    def handle_rest_exception
      begin
        yield
      rescue Exception => e
        e.response
      end
    end
end
